/**
 * 2-3-5 unity hyperspace coefficients
 */
export const UNITY = (() => {
    let unity = [-1., -1.584962500721156, -2.321928094887362]
    Object.freeze(unity)
    return unity
})()

export const logb = (base, x) => Math.log2(x) / Math.log2(base)

/**
 * find d coeff for ax+by+cz+d=0
 */
export const d_coeff = (a, b, c, x, y, z) => -(a * x + b * y + c * z);


/**
 * projection of a point (x,y,z)) along the plane (ax,by,cz) onto one of axis
 * in case with {2,3,5} unity plane is equivalent to
 * log of base (2,3,5)[axis] of (2**x * 3**y * 5**z)
 */
export const axis_intersect = (a, b, c, x, y, z, axis) => (a * x + b * y + c * z) / [a, b, c][axis];


export const bresenham_line2d = (x0, y0, x1, y1) => {
    var dx, dy, e2, error, points, sx, sy;
    x0 = Math.round(x0);
    y0 = Math.round(y0);
    x1 = Math.round(x1);
    y1 = Math.round(y1);
    dx = Math.abs(x1 - x0);
    sx = x0 < x1 ? 1 : -1;
    dy = -Math.abs(y1 - y0);
    sy = y0 < y1 ? 1 : -1;
    error = dx + dy;
    points = [];

    while (true) {
        points = points.concat([[x0, y0]]);
        if (x0 === x1 && y0 === y1) break;

        e2 = error << 1;
        if (e2 >= dy) {
            if (x0 === x1) break;
            error = error + dy;
            x0 = x0 + sx;
        }

        if (e2 <= dx) {
            if (y0 === y1) break;
            error = error + dx;
            y0 = y0 + sy;
        }
    }

    return points;
}

export const bresenham_line3d = (x0, y0, z0, x1, y1, z1) => {
    var dx, dy, dz, p1, p2, points, xs, ys, zs;
    x0 = Math.round(x0);
    y0 = Math.round(y0);
    z0 = Math.round(z0);
    x1 = Math.round(x1);
    y1 = Math.round(y1);
    z1 = Math.round(z1);
    points = [];
    points = points.concat([[x0, y0, z0]]);
    dx = Math.abs(x1 - x0);
    dy = Math.abs(y1 - y0);
    dz = Math.abs(z1 - z0);

    x1 > x0 ? xs = 1 : xs = -1;
    y1 > y0 ? ys = 1 : ys = -1;
    z1 > z0 ? zs = 1 : zs = -1;

    if (dx >= dy && dx >= dz) {
        p1 = 2 * dy - dx;
        p2 = 2 * dz - dx;

        while (x0 !== x1) {
            x0 += xs;

            if (p1 >= 0) {
                y0 += ys;
                p1 -= 2 * dx;
            }

            if (p2 >= 0) {
                z0 += zs;
                p2 -= 2 * dx;
            }

            p1 += 2 * dy;
            p2 += 2 * dz;
            points = points.concat([[x0, y0, z0]]);
        }
    } else {
        if (dy >= dx && dy >= dz) {
            p1 = 2 * dx - dy;
            p2 = 2 * dz - dy;

            while (y0 !== y1) {
                y0 += ys;

                if (p1 >= 0) {
                    x0 += xs;
                    p1 -= 2 * dy;
                }

                if (p2 >= 0) {
                    z0 += zs;
                    p2 -= 2 * dy;
                }

                p1 += 2 * dx;
                p2 += 2 * dz;
                points = points.concat([[x0, y0, z0]]);
            }
        } else {
            p1 = 2 * dy - dz;
            p2 = 2 * dx - dz;

            while (z0 !== z1) {
                z0 += zs;

                if (p1 >= 0) {
                    y0 += ys;
                    p1 -= 2 * dz;
                }

                if (p2 >= 0) {
                    x0 += xs;
                    p2 -= 2 * dz;
                }

                p1 += 2 * dy;
                p2 += 2 * dx;
                points = points.concat([[x0, y0, z0]]);
            }
        }
    }

    return points;
}

/**
 * rasterize a parallelogram on the hyperplane
 * a, b, c, d: hyperplane coeffs
 * xc, yc: x and y constraints
 * @returns points
 */
export const bresenham_plane3d = (a, b, c, d, xc, yc) => {
    let pt0, pt1;
    let points = [];
    let z = (x, y) => (-a * x - b * y - d) / c
    for (let x = -xc ; x < xc + 1; x += 1) {
        pt0 = [x, yc, z(x, yc)];
        pt1 = [x, -yc, z(x, -yc)];
        points = points.concat(bresenham_line3d(...pt0, ...pt1));
    }

    return points;
}

export default {
    UNITY,
    logb,
    d_coeff,
    axis_intersect,
    bresenham_line2d,
    bresenham_line3d,
    bresenham_plane3d
}
